<?php

function conforme($chaine){ // Le mot de passe doit correspondre à cette expression régulière
    return (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{6,}$#', $chaine)); //Il doit contenir un minuscule, majuscule un chiffre et un caractère spécial
}

?>